// console.log("hello batch 243!")*/

// [Section] Function

	// Function in javascript are lines/blocks of codes that tell our device/ application to perform a certain task when called or invoke.
	// Functions are mostly created to create a complicated tasks to run several lines of code in succession/
	// They are also used to prevent repeating lines/blocks of codes that perform the same task or function.

	// Function declaration
		// (function statements) - defines a function with specified parameters

		/*Syntax:
			function functionName(){
				code block (statements)
			}
		*/

		// function keyword - used to define a javascript functions
		// functionName - the function name. Functions are named to be able to use later in the code.
		// function block({}) - the statements which comprise the body of the function. This is where the code will be executed.

			function printName(){
				console.log("My name is John.")
				console.log("My last name is Dela Cruz.");
			}
		// Function Invocation
			// The code block and statements inside a function is not immediately executed when the function is defined/ declared. The code block and statements inside a function is executed when the function is invoked. 
			// It is common to use the term "call a function" instead of "invoke a function"

				// Let's invoke the function that we declared.
				printName(); 
				// Function are reusable.
				printName();

// [Sections] Function Declaration and Function Expression 
	// function declaration
		// A function can be created through function declaration by using the keyword function and adding function name.

		// Declared functions are not executed immediately.
		// hoisted, it will work 

		declaredFunction();

		function declaredFunction(){
			console.log("Hello World frome declaredFunction");
		}
	// function expression
		// a function can also be stored in a variable. This is called function expression. 

		// Anonymous function - functions without a name. 
		// Expression should be first

		// variableFunction();

		let variableFunction = function(){
			console.log("Hellow for variableFunction!")		
		}
		
		variableFunction()

		let funcExpression = function funcName(){
			console.log("Hello from funcExpression!")
		}

		funcExpression();

	// You can resasign declared functions and function expression to new anonymous function. 

		declaredFunction = function(){
			console.log("Updated declaredFunction");
		}

		declaredFunction();

		functionExpression = function(){
			console.log("Updated funcExpression");

		}	
		funcExpression()

		// Function expression using const keyword
		const constantFunc = function(){
			console.log("Initialized with const!")
		}

		constantFunc();


		// This will not work because this a constant
		/*constantFunc = function(){
			console.log("hi")
		}
		constantFunc();*
// [Section] Function Spring

/*
	Scope is accesibility of variable within our program.

	Javascript Variables, it has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope
*/
		
		// This variable will only be accessible within the chunk/block/braces
		{
			let localVar = "Armando Perez";
			console.log(localVar); 
		}

	let globalVar = "Mr. Worldwide"; 
	// console.log(localVar); this will not work

	// Function Scope 
	// Javascript has function scope: Each function creates a new scope
	// Variables defined inside a function are not accessible outside the function. 

	// function showNames(){
	// 	let functionLet ="Jane";
	// 	const functionConst ="John";

	// 	console.log(functionConst);
	// 	console.log(functionLet);
	// }

	// showNames();

	// The variables, functionLet, functionConst are function scoped and cannot be accessed outside of the function that they were declared
	// console.log(functionConst); Will cause error
	// console.log(functionLet);

// [Section] Nested Functions
	// You can create another function inside a function. This is called nested function.

	function myNewFunction(){
		let name = "Jane";
		console.log(name)
		function nestedFunction(){
			let nestedName ="John"

			console.log(nestedName);
			console.log(name);
		}
		nestedFunction();
	}

	myNewFunction();

	// Function and Global Scoped Variables
		// Global Scoped Variable
		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz";

			console.log(globalName);
			console.log(nameInside);
		}

		myNewFunction2() 

// [Section] Using Alert
		// aler() allows us to show a small window at the top of our browser page to show information to our users. As opposed to console.log() which only shows on the console. It allows us to show a short dialog or instruction to our users. The page will wait until the user dismiss the dialog.

		alert("Hello World"); //This will run immediately when page loads

		// Syntax: 
		// alert("<messageInString");

		function showSampleAlert(){
			alert("Hello, User!");
		}

		showSampleAlert(); 

		// Notes on use of alert()
			// Show only an alert (for short dialogs/message to the user)
			// Do now overuse aler() because the progam/js has to wait for it to be dismissed before continuing.

// [Section] prompt()

		// prompt allows us to show a small window at the top of the browser to gather user input. The input from the prompt() will be return as a String once the user dismisses the window. 

		// prompt("Enter your name: ");
		let samplePrompt = prompt("Enter your name: ");
		console.log(samplePrompt);
		console.log(typeof samplePrompt);

		/*
			Syntax: 
				prompt("<dialogInString>")
		*/

		let sampleNullPrompt = prompt("Don't enter anything.");
		console.log(sampleNullPrompt);
		console.log(typeof sampleNullPrompt);

		// You may add prompt inside a function
		function printWelcomeMessages(){
			let firstName = prompt("Enter your First Name: ");
			let lastName = prompt("Enter your Last Name: ");

			console.log("Hello, " + firstName + " " + lastName + "!");
		}

		printWelcomeMessages();

// [Section] Function Naming Convention 

		// Function names should be definitive of the tast it will perform. It usually contains a verb. 

			function getCourses(){
				let courses = ["Science 101", "Math 101", "English"]
				console.log(courses);
			}
			getCourses();

		// Avoid generic names to avoid confusion within yoyr code/program

			function getName(){
				let name ="jamie";
				console.log(name);
			}

			getName();

		// Avoid pointless and inappropriate functionn names

			function foo(){
				console.log(25%5);
			}

			foo();

		// name your function in small caps. Follow camelCasing when naming variables and functions. 

			function displayCarInfo(){
				console.log("Brand: Toyoya");
				console.log("Type; Sedan");
				console.log("Price: 1,500,000")
			}

			displayCarInfo();

